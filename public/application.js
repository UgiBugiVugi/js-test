(function () {

    'use strict';
	
	var input, button, result, chart, form, companyList, stockAPI;

    $(document).ready(function () {
        input = $('.search-input');
        button = $('.search-button');
        result = $('.resultRaw');
        chart = $('#chartDemoContainer');
		form = $('#search-form');
		
		companyList = new _app.CompanyList('stockTable');
		stockAPI = _app.StockAPI;
		
		//TODO: activate search and provide suggestions on every input, not only enter
		form.on('submit', function () {
			event.preventDefault();
			search();
		});
    });
	
	function search() {
        var companyName = input.val();
		
		if (companyName) {
			form.removeClass('has-error');
			input.tooltip('hide');
			button.button('loading');
			companyList.clear();
			result.hide();
			
	        stockAPI.lookup(companyName, function (error, symbols) {
				if (error) {
					handleError(error);
				} else {
					button.button('reset');
					for (var i = symbols.length - 1; i >= 0; i--) {
						var symbol = symbols[i].Symbol;
			            stockAPI.getQuote(symbol, function (error, quoteData) {
							companyList.addCompany(quoteData);
			            });
					}
				}
	        });
		} else {
			handleError('empty search');
		}
	}
	
	function handleError(message) {
		button.button('reset');
		form.addClass('has-error');
		
		input.attr('title', message)
			.tooltip('fixTitle')
			.tooltip('show');
			
		chart.empty();
		result.empty();
	}

}());
