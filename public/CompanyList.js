(function (_app) {

    'use strict';
	
	var CompanyList;
	
	_app.CompanyList = CompanyList;
	
	function CompanyList(id) {
		var $this = this;
		//TODO: handle multiple cloned lists, shouldn't happen in the near future
		$this._table = $('#'+id)[0];
		$this.addCompany = CompanyList$addCompany;
		$this.clear = CompanyList$clear;
	}

	function CompanyList$addCompany(company) {
		var table = this._table;
		var row = table.insertRow();
		row.innerHTML = '<td>'+ [
	        table.rows.length - 1,
	        company.Symbol,
	        company.Name,
	        company.LastPrice,
	        company.Change,
	        company.ChangePercent,
	        company.ChangeYTD,
	        company.ChangePercentYTD,
	        company.High,
	        company.Low,
	        company.Open ].join('<td>');
		$(row).click(function () {
			new Markit.InteractiveChartApi(company.Symbol, 365);
			var result = $('.resultRaw');
			result.show();
            result.empty();
            result.text(JSON.stringify(company, null, 4));
		}).hover(function () {
			this.bgColor = '#BBB';
		}, function () {
			this.bgColor = '';
		});
	}
	
	function CompanyList$clear() {
		var table = this._table;
		for (var i = table.rows.length - 1; i > 0; i--) {
			table.deleteRow(i);
		}
	}

}(_app = window._app || {}));
