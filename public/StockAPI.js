(function (_app) {

    'use strict';
	
	var StockAPI = {};
	
	_app.StockAPI = StockAPI;
	
	StockAPI.lookup = StockAPI$lookup;
	StockAPI.getQuote = StockAPI$getQuote;

	function StockAPI$lookup(companyName, callback) {
        $.getJSON('Api/v2/Lookup/json', {input: companyName}, function (data) {
			if (data.length) {
				callback(null, data);
			} else {
				callback('Lookup failed for ' + companyName);
			}
        }).error(function () {
        	callback('Lookup failed');
        });
	}
	
	function StockAPI$getQuote(symbol, callback) {
        $.getJSON('Api/v2/Quote/json', {symbol: symbol}, function (data) {
        	if (data) {
				callback(null, data);
			} else {
				callback('No data found for ' + symbol);
			}
        }).error(function () {
        	callback('Quote fetching failed');
        });
	}

}(_app = window._app || {}));
