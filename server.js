'use strict';

var path = require('path');

var express = require('express');
var compression = require('compression');
var httpProxy = require('http-proxy');

var expressApplication = express();
var proxy = httpProxy.createProxyServer();

expressApplication.use(compression());
expressApplication.use(express.static(path.join(__dirname, 'public')));

expressApplication.listen(8080, function () {
   console.log('server is up and running @ http://localhost:8080/');
});

expressApplication.get(/^\/(Api|MODApis).*$/, function (req, res) {
	// TODO: should check if the request is cached, and make the following request only when there's no cache for it, or some timeout has passed
	proxy.web(req, res, {target: 'http://dev.markitondemand.com'});
});

proxy.on('error', function(e) {
	console.error(e);
});

proxy.on('proxyRes', function (proxyRes, req, res) {
	// TODO: handle errors due to request limitation (response status 429), shuold check for 'Retry-after' header, and if recieved - make no requests to the api, until the timeout passes
	
	// TODO: add data to cache here
})
