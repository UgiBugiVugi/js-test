TASK
--------

So I made this basic proof-of-concept application that can fetch basic stock data about
companies. It gives you a very basic example on how to fetch data, via the proxy, and display it on the page.

You are free to modify what's already there or simply scrap it and start over, it's up to you. 

The data is fetched from Markit On Demand API and the documentation for that API can be found [here](http://dev.markitondemand.com/).

## Todo

+ You should implement error handling for the errors that currently are unhandled in the example. For example prevent empty searches and others. (+proxy)
+ (There is also a great need for improvements to how the data is being prestented in the GUI.
+ There is a rate limit imposed by the Rest API you will be working against, either implement a limiter on the client or something even nicer on the backend.
+ Look at the documentation for the API, there is a way to get data for a company over a longer period. Try and get that and present that in a graph of some kind. 
+ The overall structure and codestyle used in the proof-of-concept is lacking in many areas, especially if you start trying and extend it. Fix this as you see fit.
+ The goal is to show you what you know and that you have the ability to structure an application, you are free to add features by combining data from the Rest API in new ways. What you make of it is basically up to you!

## Things we look at and tips

+ How you structure your solution, even if the task is fairly limited in scope think of it as a base for an application that will go out into production and will be added to in the future.
+ Code conventions, not so much the specific one you choose but rather that you stick to it.
+ Knowledge of common programming patterns, where appropirate.
+ How dependant you are on libraries/frameworks for solving common problems. It's hard for us to judge your ability to implement application logic if most of it is the framework.
- Tooling is always welcome, if you feel you need it and have the time. Use what you feel is required to ensure code quality and automate boring tasks.
+ If you decide to copy code just know that we have a habit of finding it, so if you need to do it at the very least make it clear what parts of the code that is taken.
+ You are free to extend/change the very basic NodeJS server provided if you want to. There are limits/issues with the Rest API you will be working against and one way to deal with it could be to implement the fix in the backend.

## Technical Requirements

+ Keep in mind that you should write code that is targeting **Google Chrome 45.0** and **Firefox v41.0.1** or newer, So no need to care about legacy things.
