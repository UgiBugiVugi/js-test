README
--------
To install dependencies and start the server simply type the following. Please note that you must have Node installed
for this to work.

 ```
 npm install 
 node ./server.js
 ```

Also, you can start a watcher, that restarts the server on saved changes for development via `npm run dev` command.
 
 After that is done you should be able to access the webpage by using the following 
 url [http://localhost:8080](http://localhost:8080). 
 
Documentation
--------

 * Markit On Demand API documentation can be found [here](http://dev.markitondemand.com/) 
 * jQuery documentation can be found [here](http://api.jquery.com/)